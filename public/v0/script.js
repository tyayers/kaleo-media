(function() {


}).call(this);

// Search box functions

$( document ).ready(function() {
    $( ".search-input" ).keyup(function() {
         var currentInput = $(".search-input").val();
	 searchMovie();
    });

    $(".search-input").keypress(function(e) {
        if (e.which == 13) {
            // enter press
            searchMovie();
        }
    });

    $(".contain").on("mouseover", ".tile", function(e) {
        var id = e.currentTarget.getAttribute("data-index");
        if (id != null && id < results.length) {
            $("#movie-title").text(results[id].title);
            $("#movie-description").text(results[id].overview);
            $("#movie-popularity").text("🖒 " + results[id].vote_average);
            // $("#movie-trend").text("📈 " + results[id].popularity);
            $("#movie-details").show();
        }
    });

    function closeModal() {
        $(".modal-frame").removeClass('active');
        $(".modal-frame").addClass('leave');
        $("#stream").empty();
    }

    // $('.modal-popup').click(function() {
    //     $modal.toggleClass('active');
    //     $modal.removeClass('leave');
    // });

    $('.modal-overlay').click(function() {
        closeModal();
    });

    $('#close').click(function() {
        closeModal();
    });  

    $(document).keyup(function(e) {
        if(e.which === 27) {
            closeModal();
        }
    })    
});

var origResults = [
    {title: "Top Gear", overview: "The most overrated show on television is back!", vote_average: "8.2"},
    {title: "Saturday Night Fever Redux", overview: "You thought the dancing was over? Think again!", vote_average: "5.4"},
    {title: "Tractor Tricks", overview: "These tractors never stop!", vote_average: "9"},
    {title: "Ninja Challenge", overview: "Ninjas, ninjas everywhere, but boring.", vote_average: "3"},
    {title: "Basketball Fever", overview: "Hoop dreams in your sleep.", vote_average: "5"},
    {title: "Dance Off 5", overview: "Dancing through the night.", vote_average: "3"},
    {title: "Awards Redux", overview: "Awards are going to make us so happy", vote_average: "8"},
    {title: "Singing Masters 1", overview: "Sing it!", vote_average: "3"},
    {title: "Spy Caper", overview: "Spying is a way of life.", vote_average: "4"},
    {title: "Sing Song 3", overview: "Singing again, just like last summer.", vote_average: "3"},
    {title: "One Ok Day", overview: "This day is fine", vote_average: "2"},
    {title: "Leopards", overview: "Animals in their native habitat.", vote_average: "8"},
    {title: "Fit Fit", overview: "Get fit!", vote_average: "1"},
    {title: "Groovy Mix", overview: "Be happy and groovy!", vote_average: "9"},
    {title: "Wedding Disasters", overview: "It's a disaster", vote_average: "4"},
    {title: "Beach House Mixups", overview: "Everything's beachy", vote_average: "1"},
    {title: "Nuclear Summer", overview: "A total blow up", vote_average: "7"},
    {title: "Nuclear Summer 2", overview: "More of the same", vote_average: "6"},
    {title: "Nuclear Summer 3", overview: "Let's just do it again", vote_average: "5"}       
];

var results = origResults;

function searchToggle(obj, evt){
    var container = $(obj).closest('.search-wrapper');
    if(!container.hasClass('active')){
        container.addClass('active');
        evt.preventDefault();
    }
    else if(container.hasClass('active') && $(obj).closest('.input-holder').length == 0){
        container.removeClass('active');
        // clear input
        container.find('.search-input').val('');

        results = origResults;
        // refill list with default videos
        var temp = document.getElementsByTagName("template")[0];
        $(".row__inner").empty();
        $(".row__inner").append(temp.innerHTML);        
    }
    else {
        searchMovie();
    }
}

function searchMovie() {
    var currentInput = $(".search-input").val();
    $.get( "https://emea-poc13-prod.apigee.net/kaleo-media/search/movie?query=" + currentInput + "&apikey=WRTcsGSaR3U7hbqlTlTKmUR80Jm6m7re", function( data ) {
        var count = data.results.length;
        if (count > 10) count = 10;

        if (count > 0) {
	        results = data.results;
            $(".row__inner").empty();

            for(i=0; i < count; i++) {
                if (data.results[i].backdrop_path) {
                    $(".row__inner").append("<div class=\"tile modal-popup\" data-index=\"" + i + "\" onclick=\"stream(" + data.results[i].id + ", '" + encodeURIComponent(data.results[i].title) + "')\"><div class=\"tile__media\"><img class=\"tile__img\" src=\"http://image.tmdb.org/t/p/w185/" + data.results[i].backdrop_path + "\" alt=\"\"  /></div><div class=\"tile__details\"><div class=\"tile__title\">" + data.results[i].title + "</div></div></div>");
                }
            }
        }
    });
}

function stream(id, name) {

    $.get( "https://emea-poc13-prod.apigee.net/kaleo-media/stream/" + id + "?name=" + name + "%20trailer&apikey=WRTcsGSaR3U7hbqlTlTKmUR80Jm6m7re", function( data ) {
        $("#stream").empty();
        $("#stream").append(data);

        $(".modal-frame").toggleClass('active');
        $(".modal-frame").removeClass('leave');
    });
}

function debug(txt) {
    //$('.contain').append("<div class='row'  style='width:100%;background:orange;padding:3px;font-size:8px;word-wrap: break-word; border: 1rem solid;'>" + txt + "</div>");
}