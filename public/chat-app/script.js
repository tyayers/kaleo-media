(function(){
  
  var chat = {
    messageToSend: '',
    messageResponses: [
      'Why did the web developer leave the restaurant? Because of the table layout.',
      'How do you comfort a JavaScript bug? You console it.',
      'An SQL query enters a bar, approaches two tables and asks: "May I join you?"',
      'What is the most used language in programming? Profanity.',
      'What is the object-oriented way to become wealthy? Inheritance.',
      'An SEO expert walks into a bar, bars, pub, tavern, public house, Irish pub, drinks, beer, alcohol'
    ],
    imagesrc: '',
    init: function() {
      this.cacheDOM();
      this.bindEvents();
      this.render();
    },
    cacheDOM: function() {
      this.$chatHistory = $('.chat-history');
      this.$button = $('button');
      this.$textarea = $('#message-to-send');
      this.$imageinput = $('#image-input');
      this.$chatHistoryList =  this.$chatHistory.find('ul');
    },
    bindEvents: function() {
      this.$button.on('click', this.addMessage.bind(this));
      this.$textarea.on('keyup', this.addMessageEnter.bind(this));
      this.$imageinput.on('change', this.addImage.bind(this));
    },
    render: function() {
      this.scrollToBottom();
      if (this.messageToSend.trim() !== '') {
        var template = Handlebars.compile( $("#message-template").html());
        var context = { 
          messageOutput: this.messageToSend,
          imageSrc: this.imagesrc,
          time: this.getCurrentTime()
        };

        this.$chatHistoryList.append(template(context));
        this.scrollToBottom();
        this.$textarea.val('');

        axios.get('https://emea-poc13-prod.apigee.net/kaleo-media/search/movie?query=' + encodeURI(this.messageToSend) + '&apikey=PGGjtDM10W8BDLuNbT86CsxJGBdoxyG7')
        .then(function (response) {
          console.log(response);

          if (response.data.results.length > 0) {
            axios.get('https://emea-poc13-prod.apigee.net/kaleo-media/movie/' + response.data.results[0].id + '/recommendations?apikey=PGGjtDM10W8BDLuNbT86CsxJGBdoxyG7').then(function (response2) {
              console.log(response2);
              var message = "I didn't find any recommendations, that's strange!";

              if (response2.data.results.length > 0) {
                message = "I found these recommendations: ";
                for (i=0; i<response2.data.results.length; i++) {
                  message += response2.data.results[i].title + ", ";
                }
              }
              var templateResponse = Handlebars.compile( $("#message-response-template").html());
              var contextResponse = { 
                response: message,
                time: this.getCurrentTime()
              };
              
              setTimeout(function() {
                this.$chatHistoryList.append(templateResponse(contextResponse));
                this.scrollToBottom();
              }.bind(this), 1500);              
            }.bind(this));            
          }
          else {
            var message = "I didn't find any recommendations, that's strange!";
            var templateResponse = Handlebars.compile( $("#message-response-template").html());
            var contextResponse = { 
              response: message,
              time: this.getCurrentTime()
            };
            
            setTimeout(function() {
              this.$chatHistoryList.append(templateResponse(contextResponse));
              this.scrollToBottom();
            }.bind(this), 1500);              
          }
        }.bind(this));
      }
    },
    addMessage: function() {
      this.messageToSend = this.$textarea.val()
      this.render();         
    },
    addImage: function() {
      if (this.$imageinput[0].files && this.$imageinput[0].files[0]) {

        var reader = new FileReader();
        var me = this;
        reader.onload = function(e) {

          me.imagesrc = e.target.result;
          me.render(); 
        };

        reader.readAsDataURL(this.$imageinput[0].files[0]);
        
      }              
    },    
    addMessageEnter: function(event) {
        // enter was pressed
        if (event.keyCode === 13) {
          this.addMessage();
        }
    },
    scrollToBottom: function() {
       this.$chatHistory.scrollTop(this.$chatHistory[0].scrollHeight);
    },
    getCurrentTime: function() {
      return new Date().toLocaleTimeString().
              replace(/([\d]+:[\d]{2})(:[\d]{2})(.*)/, "$1$3");
    },
    getRandomItem: function(arr) {
      return arr[Math.floor(Math.random()*arr.length)];
    }
    
  };
  
  chat.init();
  
  var searchFilter = {
    options: { valueNames: ['name'] },
    init: function() {
      var userList = new List('people-list', this.options);
      var noItems = $('<li id="no-items-found">No items found</li>');
      
      userList.on('updated', function(list) {
        if (list.matchingItems.length === 0) {
          $(list.list).append(noItems);
        } else {
          noItems.detach();
        }
      });
    }
  };
  
  searchFilter.init();
})();
