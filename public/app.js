const routes = [
    { path: '/', name: "home", component: Vue.component("home-v1") },    
    { path: '/v2', component: Vue.component("home-v2") },    
    { path: '/v3', component: Vue.component("home-v3") }
];

const router = new VueRouter({
  routes
});

Vue.use(VueMaterial.default);

var app = new Vue({
    router: router,
    el: '#app',
    data: {
        menuVisible: false        
    }
});

axios.defaults.headers.common["Content-Type"] = "application/json";
axios.defaults.baseURL = "";


(function() {


}).call(this);

// Search box functions

$( document ).ready(function() {
    $( ".search-input" ).keyup(function() {
         var currentInput = $(".search-input").val();
	 searchMovie();
    });

    $(".search-input").keypress(function(e) {
        if (e.which == 13) {
            // enter press
            searchMovie();
        }
    });

    $(".contain").on("mouseover", ".tile", function(e) {
        var id = e.currentTarget.getAttribute("data-index");
        if (id != null && id < results.length) {
            $("#movie-title").text(results[id].title);
            $("#movie-description").text(results[id].overview);
            $("#movie-popularity").text("🖒 " + results[id].vote_average);
            // $("#movie-trend").text("📈 " + results[id].popularity);
            $("#movie-details").show();
        }
    });

    function closeModal() {
        $(".modal-frame").removeClass('active');
        $(".modal-frame").addClass('leave');
        $("#stream").empty();
    }

    // $('.modal-popup').click(function() {
    //     $modal.toggleClass('active');
    //     $modal.removeClass('leave');
    // });

    $('.modal-overlay').click(function() {
        closeModal();
    });

    $('#close').click(function() {
        closeModal();
    });  

    $(document).keyup(function(e) {
        if(e.which === 27) {
            closeModal();
        }
    })    
});


function searchToggle(obj, evt) {
    var container = $(obj).closest('.search-wrapper');
    if(!container.hasClass('active')){
        container.addClass('active');
        evt.preventDefault();
    }
    else if(container.hasClass('active') && $(obj).closest('.input-holder').length == 0){
        container.removeClass('active');
        // clear input
        container.find('.search-input').val('');

        // refill list with default videos
        var temp = document.getElementsByTagName("template")[0];
        $(".row__inner").empty();
        $(".row__inner").append(temp.innerHTML);        
    }
    else {
        searchMovie();
    }
}

function searchMovie() {
    // var currentInput = $(".search-input").val();
    // $.get( "https://emea-poc13-prod.apigee.net/kaleo-media/search/movie?query=" + currentInput + "&apikey=WRTcsGSaR3U7hbqlTlTKmUR80Jm6m7re", function( data ) {
    //     var count = data.results.length;
    //     if (count > 10) count = 10;

    //     if (count > 0) {
	//         results = data.results;
    //         $(".row__inner").empty();

    //         for(i=0; i < count; i++) {
    //             if (data.results[i].backdrop_path) {
    //                 $(".row__inner").append("<div class=\"tile modal-popup\" data-index=\"" + i + "\" onclick=\"stream(" + data.results[i].id + ", '" + encodeURIComponent(data.results[i].title) + "')\"><div class=\"tile__media\"><img class=\"tile__img\" src=\"http://image.tmdb.org/t/p/w185/" + data.results[i].backdrop_path + "\" alt=\"\"  /></div><div class=\"tile__details\"><div class=\"tile__title\">" + data.results[i].title + "</div></div></div>");
    //             }
    //         }
    //     }
    // });
}

function stream(id, name) {

    $.get( "https://emea-poc13-prod.apigee.net/kaleo-media/stream/" + id + "?name=" + name + "%20trailer&apikey=WRTcsGSaR3U7hbqlTlTKmUR80Jm6m7re", function( data ) {
        $("#stream").empty();
        $("#stream").append(data);

        $(".modal-frame").toggleClass('active');
        $(".modal-frame").removeClass('leave');
    });
}

