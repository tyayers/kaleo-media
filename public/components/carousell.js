Vue.component('carousell', {
  template: `
    <div class="contain" style="margin-bottom: auto;">
        <div class="row">
            <div class="row__inner">
                <div v-for="video in items" class="tile animated fadeIn" data-index="0" v-on:click="streamMovie(video.id, video.title)">
                    <div class="tile__media">
                    <img class="tile__img" v-bind:src="video.imageUrl" alt=""  />
                    </div>
                    <div class="tile__details">
                    <div class="tile__title">
                        {{video.title}}
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    `,
    props: {
        items: Array
    },
    methods: {
        streamMovie(id, name) {
            $.get( "https://emea-poc13-prod.apigee.net/kaleo-media/stream/" + id + "?name=" + encodeURIComponent(name) + "%20trailer&apikey=WRTcsGSaR3U7hbqlTlTKmUR80Jm6m7re", function( data ) {
                $("#stream").empty();
                $("#stream").append(data);
                $(".modal-frame").toggleClass('active');
                $(".modal-frame").removeClass('leave');
            });
        }
    }
});    
