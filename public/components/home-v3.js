Vue.component('home-v3', {
  template: `
    <div class="contain" style="margin-top: 100px; margin-bottom: auto;">
        <div class="search-wrapper" v-bind:class="{ active: searchVisible }">
            <div class="input-holder">
                <input type="text" v-model="searchText" v-on:keyup="searchMovie" class="search-input" placeholder="Type to search" />
                <button class="search-icon" onclick="searchToggle(this, event);"><span></span></button>
            </div>
            <span class="close" v-on:click="searchToggle()"></span>
        </div>
        
        <br>
        
        <div v-if="videos.length>0" class="animated slideInLeft" style="position: relative; top: 60px; max-height: 220px;">
            <h3>Search Results</h3>
            <carousell style="position: relative; top: -70px" v-bind:items="videos"></carousell>
        </div>

        <div class="animated slideInLeft" style="position: relative; top: 60px">
            <h3>New</h3>
            <carousell style="position: relative; top: -70px" v-bind:items="nowPlayingVideos"></carousell>
        </div>

        <div class="animated slideInLeft" style="position: relative; top: -60px">
            <h3>Popular</h3>
            <carousell style="position: relative; top: -70px" v-bind:items="popularVideos"></carousell>
        </div>

        <div class="animated slideInLeft" style="position: relative; top: -190px">
            <h3>Top Rated</h3>
            <carousell style="position: relative; top: -70px" v-bind:items="highestRatedVideos"></carousell>
        </div>

        <df-messenger
        chat-icon="img/movie-icon4.png"
        intent="WELCOME"
        chat-title="Movie Genius"
        agent-id="37fffbfd-dbd3-467a-b1c0-932f9be328f8"
        language-code="en"
        ></df-messenger>              
    </div>
    `,
    data: function() {
        return {
            searchText: "",
            searchVisible: true,
            videos: [],
            popularVideos: [],
            nowPlayingVideos: [],
            highestRatedVideos: [],

        }
    },
    created () {
        this.init();
    }, 
    methods: {
        init() {
            var localVideos = this.videos;
            var localPopularVideos = this.popularVideos;
            var localNowPlayingVideos = this.nowPlayingVideos;
            var localHighestRatedVideos = this.highestRatedVideos;
            var me = this;
            $.get( "https://emea-poc13-prod.apigee.net/kaleo-media/movie/popular?apikey=WRTcsGSaR3U7hbqlTlTKmUR80Jm6m7re", function( data ) {
                me.fillCarousell(data.results, localPopularVideos);
            });
            $.get( "https://emea-poc13-prod.apigee.net/kaleo-media/movie/now_playing?apikey=WRTcsGSaR3U7hbqlTlTKmUR80Jm6m7re", function( data ) {
                me.fillCarousell(data.results, localNowPlayingVideos);
            });
            $.get( "https://emea-poc13-prod.apigee.net/kaleo-media/movie/top_rated?apikey=WRTcsGSaR3U7hbqlTlTKmUR80Jm6m7re", function( data ) {
                me.fillCarousell(data.results, localHighestRatedVideos);
            });                                    
        },
        fillCarousell(results, carousell) {
            var count = results.length;
            if (count > 10) count = 10;

            if (count > 0) {
                results = results;
                carousell.length = 0;

                for(i=0; i < count; i++) {
                    if (results[i].backdrop_path) {
                        
                        carousell.push({
                            id: results[i].id,
                            title: results[i].title,
                            imageUrl: "http://image.tmdb.org/t/p/w185/" + results[i].backdrop_path
                        });
                    }
                }
            }
        },
        searchMovie() {
            if (this.searchText == "") {
                this.videos.length = 0;
            }
            else {
                var localVideos = this.videos;
                $.get( "https://emea-poc13-prod.apigee.net/kaleo-media/search/movie?query=" + this.searchText + "&apikey=WRTcsGSaR3U7hbqlTlTKmUR80Jm6m7re", function( data ) {
                    var count = data.results.length;
                    if (count > 10) count = 10;

                    if (count > 0) {
                        results = data.results;
                        localVideos.length = 0;

                        for(i=0; i < count; i++) {
                            if (data.results[i].backdrop_path) {
                                
                                localVideos.push({
                                    id: data.results[i].id,
                                    title: data.results[i].title,
                                    imageUrl: "http://image.tmdb.org/t/p/w185/" + data.results[i].backdrop_path
                                });
                            }
                        }
                    }
                });
            }
        },
        streamMovie(id, name) {
            $.get( "https://emea-poc13-prod.apigee.net/kaleo-media/stream/" + id + "?name=" + encodeURIComponent(name) + "%20trailer&apikey=WRTcsGSaR3U7hbqlTlTKmUR80Jm6m7re", function( data ) {
                $("#stream").empty();
                $("#stream").append(data);
                $(".modal-frame").toggleClass('active');
                $(".modal-frame").removeClass('leave');
            });
        },
        searchToggle() {
            this.searchVisible = !this.searchVisible;
            
            if (!this.searchVisible) {
                this.searchText = "";
                this.videos.length = 0;
            }
        }
    }
});    
