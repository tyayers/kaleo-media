# Start from a Debian image with the latest version of Go installed
# and a workspace (GOPATH) configured at /go.
FROM golang

# Copy the local package files to the container's workspace.
WORKDIR /go/src/kaleo-media
COPY . .

# Build the outyet command inside the container.
# (You may fetch or manage dependencies here,
# either manually or with a tool like "godep".)
RUN go get -u github.com/gin-gonic/gin
RUN go install kaleo-media

# Run the outyet command by default when the container starts.
ENTRYPOINT /go/bin/kaleo-media

# Document that the service listens on port 8080.
EXPOSE 8080