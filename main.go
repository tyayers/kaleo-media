package main

import (
	"github.com/gin-gonic/gin"  
)

type Todo struct {
    Name      string
    Completed bool
}
 
type Todos []Todo

func main() {

  r := gin.Default()

  r.Static("/app", "./public")

	r.GET("/ping", func(c *gin.Context) {
		c.String(200, "pong")
	})

  r.Run(":8080")
}