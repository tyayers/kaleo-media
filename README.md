# Kaleo Media

<b>LIVE demo version:</b> [https://tyayers.gitlab.io/kaleo-media/frontend/media-center-app/](https://tyayers.gitlab.io/kaleo-media/frontend/media-center-app/)

This demo shows how APIs and API proxies can power a media content, search, streaming and recommendation platform.  No additional microservices are needed, just intelligent API proxies that are connecting and leveraging different endpoints and services to deliver maximum user content and value.

<b>DEPLOY now to Google Cloud Run:</b>

[![Run on Google Cloud](https://deploy.cloud.run/button.svg)](https://deploy.cloud.run?git_repo=https://gitlab.com/tyayers/kaleo-media.git)

## Architecture

Here is a diagram of the architecture used in this demo:

<img src="img/kaleo-media-architecture.png" height="400" />

## Scenario

The fictional Hooli Streaming app offers media content from a backend content platform (in this demo YouTube, but could be anything), and also uses search and recommendation services from a 3rd party platform (in this case themoviedb.com, but could be anything).  The app has 3 versions, each with more functionality and integration than the last.  You can select the version of the app in the upper-left corner (v1, v2, v3).  All 3 versions of the app just use the API proxy to integrate with all of the needed services, without any additional code or microservices (low-code / no-code API orchestration).

### v1
The v1 version of the app offers basic media search & streaming functionality.  You can search for media content, and stream selected content.

<img src="img/hooli-app-v1.png" width="500"/>

### v2
The v2 version of the app adds a DialogFlow / Google Assistant endpoint, which seamlessly integrates with the backend services to provide movie recommendation functionality.

The DialogFlow bot / Google Assistant action is published under the name <b>Movie Genius</b>, so just ask Google Assistant to talk to <b>Movie Genius</b>, and you will be talking to the Hooli Streaming API service, just like the web app.  The bot is also integrated in the web app at the bottom of the page through the <b>DialogFlow Messenger</b> interface.

<img src="img/hooli-app-v2.png" width="500"/>

### v3
The v3 version of the app adds further new release, top-rated, and popular content categories, based on view data, and again by offering additional API endpoints exposing that data to apps.

<img src="img/hooli-app-v3.png" width="500"/>

## Backend Services

The great thing about APIs is that the backend services can change as needed.  For this demo I used themoviedb.com for search and recommendation data, and youtube for media content.

## Demo Screencast

<a href="screencast/Apigee Kaleo Media Showcase.mp4?raw=true" target="_blank"><img src="screencast/screencase-cover.png" height="350" /></a>


# Credits
Netflix codepen: https://codepen.io/joshhunt/pen/LVQZRa

Search codepen: https://codepen.io/alexpopovich/pen/PWLRgV

TheMovieDb for the backend: https://www.themoviedb.org/

